package handler

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/mnovotny/sovywebfiles/scrypt"
)

func RootHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprintf(w, "{\"status\":\"ok\"}")
}

func RandomString(lenght int) string {
	const charset = "abcdefghijkLMNOPQRSTUVmXYZ0123" + "lmnopqrstuv7xyzABCDEFGHIJK456789"
	var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
	out := make([]byte, lenght)
	for i := range out {
		out[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(out)
}

func CreateHashHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	var input, output Block
	err := json.NewDecoder(r.Body).Decode(&input)
	if handleErr(err, w) {
		return
	}
	if input.Salt != "" {
		input.Data = input.Salt + input.Data
	}
	output.Data = scrypt.Hasher(input.Data)
	fmt.Fprintf(w, errorStatus("hash", output.Data))
}

func TokenHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	var output Block
	output.Data = RandomString(32)
	fmt.Fprintf(w, errorStatus("token", output.Data))
}

func EncryptHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	var input Block
	var output CryptData
	err := json.NewDecoder(r.Body).Decode(&input)
	if handleErr(err, w) {
		return
	}
	if input.Salt != "" {
		input.Key = scrypt.Hasher(input.Salt + input.Key)
	} else {
		input.Key = scrypt.Hasher(input.Key)
	}
	fmt.Println(input)
	output.Data, err = scrypt.Encrypt([]byte(input.Key), []byte(input.Data))
	final, err := json.Marshal(output)
	if handleErr(err, w) {
		return
	}
	fmt.Fprintf(w, string(final))
}

func DecryptHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	var input CryptData
	err := json.NewDecoder(r.Body).Decode(&input)
	if handleErr(err, w) {
		return
	}
	if input.Salt != "" {
		input.Key = scrypt.Hasher(input.Salt + input.Key)
	} else {
		input.Key = scrypt.Hasher(input.Key)
	}
	output, err := scrypt.Decrypt([]byte(input.Key), []byte(input.Data))
	errorStatus("data", string(output))
}

type CryptData struct {
	Data []byte `json:"data,omitempty"`
	Key  string `json:"key,omitempty"`
	Salt string `json:"salt,omitempty"`
}

type Block struct {
	Data string `json:"data,omitempty"`
	Key  string `json:"key,omitempty"`
	Salt string `json:"salt,omitempty"`
}

func errorStatus(key string, msg string) string {
	return "{\"" + key + "\":\"" + msg + "\"}"
}

func handleErr(err error, w http.ResponseWriter) bool {
	if err != nil {
		panic(err)
		w.WriteHeader(500)
		fmt.Fprintf(w, errorStatus("error", "internal"))
		return true
	}
	return false
}
