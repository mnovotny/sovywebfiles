package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/mnovotny/sovywebfiles/handler"
)

func main() {
	var port string = ":8080"
	fmt.Println("Hello GO on port ", port)

	r := mux.NewRouter()
	r.HandleFunc("/", handler.RootHandler)
	r.HandleFunc("/token", handler.TokenHandler).Methods("GET") //free
	r.HandleFunc("/hash", handler.CreateHashHandler).Methods("POST")
	r.HandleFunc("/decrypt", handler.DecryptHandler).Methods("POST")
	r.HandleFunc("/encrypt", handler.EncryptHandler).Methods("POST")
	http.Handle("/", r)

	http.ListenAndServe(port, r)
}
